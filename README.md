Physics-CompClass

Rechnergestützte Physik

Willkommen zur rechnergestützten Physik! Dies ist das Repository mit dem Lehrplan, den Problemsets und den Aufgaben.

Kontakt: Alexander Scherer Krefeld Nachhilfe
Homepage: https://nachhilfekrefeld.com


Themen

Woche 1
-Einführung
-Grundlagen der Programmierung
**Woche 2**
-Objektorientierte Programmierung
-Fehlerakkumulation
**Woche 3**
-Numerische Werkzeuge
-Plotten
-Wie Git funktioniert
**Woche 4**
-Zufallszahlen
-Monte Carlo - Zufälliger Spaziergang
-Projekte - IPython-Magie
**Woche 5**
-Integrationsregeln - Anmerkungen
-Monte-Carlo-Integration - Anmerkungen
-Numerische Differenzierung
**Woche 6**
-Vektorisierung - Anmerkungen
-Lineare Algebra - String-Massen: Klassik - Finale - Sympy
-Lineare Regression - Spline-Passung
**Woche 7**
-Strukturierte Tabellendaten - Arbeitsblatt
-Histogramme und Schnitte
**Woche 8**
-Erzeugen von Verteilungen
-Minimierung und Anpassung von Verteilungen
-Montage-Werkzeuge
**Woche 9**
-Vertrauensintervalle
-Markov-Kette Monte Carlo
-Leistungs-Computing
**Woche 10**
-Einführung in ODEs - Profiling von Code, Lesen und Schreiben von Dateien
-Der Runge-Kutta-Algorithmus
-ODE-Probleme lösen
**Woche 11: Fourier-Reihen**
-Fourier-Reihe
-Schnelle Fourier-Transformation
**Woche 12: Verschiedene Themen**
-CuPy-Fraktal
-PyBind11 und Numba
-Anpassung Überarbeitet
-GUIs
-Signalfilterung
**Woche 13: Rückblick**
-Rückblick
**Woche 14**
-Statische Berechnungsdiagramme
-Maschinelles Lernen - MINST-Datensatz



Kontakt: Alexander Scherer Krefeld Nachhilfe
Homepage: https://nachhilfekrefeld.com

ESCAPE OSS & Physics-CompClass

Das Open-Source-Repository für wissenschaftliche Software und Dienste ESCAPE (Open-source Scientific Software and Service Repository, OSSR) ist ein nachhaltiges Open-Access-Repository zur gemeinsamen Nutzung wissenschaftlicher Software und Dienste für die Wissenschaftsgemeinschaft und zur Ermöglichung einer offenen Wissenschaft. Es beherbergt wissenschaftliche Software und Dienste für die Astroteilchenphysik zur Datenverarbeitung und -analyse sowie Testdatensätze, Dokumentation zur Benutzerunterstützung, Tutorials, Präsentationen und Schulungsaktivitäten.
Es wird einen echten datengesteuerten kooperativen Multi-Messenger-Ansatz auf der Grundlage der Anforderungen des FAIR-Prinzips ermöglichen und Teil des globalen Dienstleistungskatalogs der EOSC werden. In einer gemeinsamen Anstrengung aller ESCAPE-Partner werden gemeinsame und innovative Ansätze gefördert werden. 
Die auf der ESCAPE OSSR verfügbare Software, wissenschaftliche Werkzeuge und andere Dienste sind für alle offen zugänglich und ermöglichen es den Benutzern, auf Multi-Messenger- und Multi-Domain-Software zuzugreifen, um Open-Access-Datensätze abzubauen. Dies garantiert die gegenseitige Befruchtung und ein offenes Innovationsumfeld für wissenschaftliche Interoperabilität und die Wiederverwendung von Software.

Wer sind die Zielbenutzer?
Die ESCAPE OSSR inklusive Physics-CompClass wird so eingerichtet, dass jeder in der wissenschaftlichen Gemeinschaft in der Lage ist, die Software und Dienste zu nutzen. Über das Hauptziel der Wiederverwendung hinaus wird die Verbreitung bestehender Lösungen die Schaffung neuer Lösungen in der Astroteilchenphysik-Gemeinschaft und angrenzenden wissenschaftlichen Bereichen fördern. Als Endbenutzer werden vor allem Einzelpersonen und Institutionen identifiziert, die bereit sind, innovativ tätig zu werden.

Was ist der Mehrwert?
Gegenwärtig gibt es kein einzelnes Repository für das eng verwandte wissenschaftliche Gebiet der Astrophysik, Astroteilchenphysik und Teilchenphysik. ESCAPE OSSR wird dieses Repository mittels Physics-CompClass generieren und einen einfachen Zugang zu den in diesem Bereich entwickelten Algorithmen, Softwarepaketen und Dienstleistungen ermöglichen, die jedoch auch für andere wissenschaftliche Probleme genutzt werden können. Zum ersten Mal vereint ESCAPE OSSR das Fachwissen weltweit führender Forschungsinfrastrukturen und -institutionen auf den Gebieten der Astrophysik, Astroteilchenphysik und Teilchenphysik. Sie streben gemeinsam danach, qualitativ hochwertige, gut dokumentierte und einfach zu bedienende Softwarelösungen auf harmonisierte Weise unter Verwendung einer gut definierten Schnittstelle auszutauschen.

Kontakt: Alexander Scherer Krefeld Nachhilfe

## **Umgezetzt mit ESCAPE OSS und CompClass:**
https://projectescape.eu/services/open-source-scientific-software-and-service-repository
https://github.com/henryiii/compclass

